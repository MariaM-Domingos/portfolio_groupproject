﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class DataPersistence : MonoBehaviour
{
    private GameObject player;
    private PhoneHandler inventory;
    private struct PlayerDetails
    {
        public float sanity;
        public int games;
        public Vector3 position;
        public int[] items;
    }
    [SerializeField] private CollectablesClass[] prefabs;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        inventory = GameObject.FindGameObjectWithTag("Scripts").GetComponent<PhoneHandler>();
        //if (GameObject.FindGameObjectWithTag("Player").name.Contains("Mini Game"))
        //{
        //    LoadSanity();
        //}
        //else
        //{
            LoadCode();
        //}
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SaveCode()
    {
        
        PlayerDetails playerDetails = new PlayerDetails
        {
            sanity = Player.sanity,
            games = Player.gamesplayed,
            position = Player.position,
            items = inventory.itemsID
        };


        string jsonString = JsonUtility.ToJson(playerDetails);

        File.WriteAllText(Application.dataPath + "/DataPersistence.json", jsonString);
        Debug.Log("Saved");
    }

    public void LoadCode()
    {

        Debug.Log("Loaded");
        if (File.Exists(Application.dataPath + "/DataPersistence.json"))
        {
            string saveString = File.ReadAllText(Application.dataPath + "/DataPersistence.json");

            PlayerDetails playerDetails = JsonUtility.FromJson<PlayerDetails>(saveString);
            player.gameObject.transform.position = playerDetails.position;
            Player.sanity = playerDetails.sanity;
            Player.gamesplayed = playerDetails.games;

            for (int i = 0; i < inventory.slots.Length; i++) 
            {
                if (prefabs[0].code == playerDetails.items[i])
                {
                    inventory.isFull[i] = true;
                    Instantiate(prefabs[0].itemButton, inventory.slots[i].transform, false);
                    inventory.itemsID[i] = playerDetails.items[i];
                }
            }
        }

        // check if won
        if (Player.won == true)
        {
            Player.sanity += MiniGameHandler.winVal;
            Player.gamesplayed += 1;
            Debug.Log("won val added");
        }
        if (Player.lost==true)
        {
            Player.sanity -= MiniGameHandler.loseVal;
            Player.gamesplayed += 1;
            Debug.Log("lost val added");
        }
    }
}

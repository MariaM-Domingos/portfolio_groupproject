using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterialColor : MonoBehaviour
{
    private Renderer mesh;
    private Color initialMaterial;
    [SerializeField] [Range(0f, 1f)] private float sanity;
    [SerializeField] Color color;
    private MetersHandler _sanity;
    // Start is called before the first frame update
    void Start()
    {
        mesh = GetComponent<Renderer>();
        _sanity = GameObject.FindGameObjectWithTag("Scripts").GetComponent<MetersHandler>();
        initialMaterial = mesh.material.color;
    }

    // Update is called once per frame
    void Update()
    {
        sanity = _sanity.GetPlayerSanity();
        UpdateColor(sanity);
    }

    private void UpdateColor(float value)
    {
        Color.Lerp(mesh.material.color, color, value);
    }
}

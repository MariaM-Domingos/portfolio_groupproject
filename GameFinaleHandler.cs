using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameFinaleHandler : MonoBehaviour
{
    [SerializeField] GameObject gameOver;
    [SerializeField] GameObject gameWon;
    [SerializeField] private GameObject dot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.sanity <= 0)
        {
            GameOver();
        }
      
    }

    void GameOver()
    {
        gameOver.SetActive(true);
        dot.SetActive(false);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0;
    }

    public void GameWon()
    {
        gameWon.SetActive(true);
        dot.SetActive(false);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0;
    }
    public void ReturnToMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    private float lookSensitivity,
                xRot;
    private Transform playerTransform;
    [SerializeField] private Transform itemLoc;

    void Start()
    {
        xRot = 0;
        lookSensitivity = 300;
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        if(itemLoc != null) 
        {
            LockCameraOnItem();
        }
        else
        {
            Rotate();
        }
        
    }

    void Rotate()
    {

        float mouseX = Input.GetAxis("Mouse X") * lookSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * lookSensitivity * Time.deltaTime;

        xRot -= mouseY;
        xRot = Mathf.Clamp(xRot, -80f, 45f);

        base.transform.localRotation = Quaternion.Euler(xRot, 0, 0);

        playerTransform.transform.Rotate(Vector3.up * mouseX);
    }

    void LockCameraOnItem()
    {
        if (itemLoc.transform.childCount > 0)
        {
            Camera.main.transform.rotation = Quaternion.Euler(0, playerTransform.rotation.eulerAngles.y, playerTransform.rotation.eulerAngles.z);
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Rotate();
        }
    }
}

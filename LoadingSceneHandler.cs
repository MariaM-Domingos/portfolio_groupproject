﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;


public class LoadingSceneHandler : MonoBehaviour
{
    private bool loadScene, canLoad;
    public Slider FillInBar;
    private float timer;
    public int SceneNumber;
   
    void Start()
    {
        Time.timeScale = 1;
        loadScene = false;
    }

    void Update()
    {
        if (!loadScene)
        {
            loadScene = true;
            StartCoroutine(LoadYourAsyncScene());
        }
        
    }
    IEnumerator LoadYourAsyncScene()
    {
        while ( timer < 1)
        {
            float randNumb = Random.Range(0, .3f);
            timer += randNumb;
            FillInBar.value += randNumb;
            randNumb = Random.Range(.2f, .5f);
            yield return new WaitForSeconds(randNumb);

        }
        
        if (timer > 1)
        {
            canLoad = true;
        }
        
        // Wait until the asynchronous scene fully at loads
        if (canLoad) {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(SceneNumber);
            while (!asyncLoad.isDone)
            {

                yield return null;
            }
        }
    }       
}


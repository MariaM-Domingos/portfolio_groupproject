﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MetersHandler : MonoBehaviour
{
    [SerializeField] private Slider sanityBar;
    [SerializeField] float value /*= -0.0002f*/;

    void Start()
    {
        //sanityBar = null;
    }

    void Update()
    {
        DeploySanityMeter(value);
    }
    public void SetSanityMeter(float value)
    {
        if (GetPlayerSanity() > 0)
        {
            SetPlayerSanity(value);
            sanityBar.value = GetPlayerSanity();
        }
        else
        {
            SetPlayerSanity(0);
        }
       
    }

    public void DeploySanityMeter(float value)
    {
        if (Time.deltaTime < 1 && Time.deltaTime !=0)
        {
            
            SetSanityMeter(value);
        }
    }

    //Player Sanity Manipulators
    public float GetPlayerSanity()
    {
        return Player.sanity;
    }
    public void SetPlayerSanity(float value)
    {
        Player.sanity += value;
    }
}

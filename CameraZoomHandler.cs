using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomHandler : MonoBehaviour
{
    [SerializeField] private float zoomVal;
    [SerializeField] private float speed;
    private float initial;
    private bool isZoomed;
  

    // Start is called before the first frame update
    void Start()
    {
        zoomVal = 20;
        speed = 5;
        isZoomed = false;
        initial = 60;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(1)) //right mouse side
        {
            isZoomed = !isZoomed;
        }
            Scroll();
    }
    
    void Scroll()
    {
            if (isZoomed)
            {
                Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, zoomVal, Time.deltaTime * speed);
            }
            else
            {
                Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, initial, Time.deltaTime * speed);
            }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PChandler : MonoBehaviour
{
    private GameObject player;
    private float distance;
    [SerializeField] private float howFarAway;
    private DataPersistence data;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        data = GameObject.FindGameObjectWithTag("Scripts").GetComponent<DataPersistence>();
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(player.transform.position, this.transform.position);
    }

    private void OnMouseOver()
    {
        RaycastHit hit;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag.Equals("Computer"))
            {
                if (Input.GetKey(KeyCode.E) && distance < howFarAway)
                {
                    data.SaveCode();
                    SceneManager.LoadScene(3);
                }
            }
        }
    }
}

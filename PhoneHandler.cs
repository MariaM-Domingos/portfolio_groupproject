﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class PhoneHandler: MonoBehaviour
{
    public bool[] isFull;
    public GameObject[] slots;
    public int[] itemsID;
    private bool isActive;
    [SerializeField] private GameObject homeScreen;
    [SerializeField] private GameObject contactScreen;
    [SerializeField] private GameObject[] messageScreen;
    [SerializeField] private GameObject centerDot;
    [SerializeField] private GameObject messageNotifier;
    private MessangerHandler messanger;

    void Start()
    {
        isActive = false;
        messanger = GameObject.FindGameObjectWithTag("Scripts").GetComponent<MessangerHandler>();
    }

    void Update()
    {
        PhoneToggle();
    }
     
    public void AddToInventory(GameObject icon, int id)
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (isFull[i] == false)
            {
                isFull[i] = true;
                Instantiate(icon, slots[i].transform, false);
                itemsID[i] = id;
                break;
            }
        }
    }
    void PhoneToggle()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {

            isActive = !isActive;
            if (isActive) 
            {
                centerDot.SetActive(false);
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                homeScreen.SetActive(true);
                
               Time.timeScale = 0;
            }
            else
            {
                centerDot.SetActive(true);
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                homeScreen.SetActive(false);
                
                Time.timeScale = 1;
            }
            for (int i = 0; i < messageScreen.Length; i++)
            {
                if (messageScreen[i].activeInHierarchy == true || contactScreen.activeInHierarchy == true)
                {
                    messageScreen[i].SetActive(false);
                    contactScreen.SetActive(false);
                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.visible = false;
                    homeScreen.SetActive(false);

                    Time.timeScale = 1;
                }
            }            
        }
    }
    public void MessagerPage()
    {
        homeScreen.SetActive(false);
        contactScreen.SetActive(true);
        messageNotifier.SetActive(false);

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            contactScreen.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1;
            centerDot.SetActive(true);
            Cursor.visible = false;
        }
    }

    public void ReturnToHomeScreen()
    {
        contactScreen.SetActive(false);
        homeScreen.SetActive(true);
    }


    public void ReturnToContactScreen(int i)
    {
        messanger.messageScreen[i].SetActive(false);
        contactScreen.SetActive(true);

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;


public class MainMenuHandler : MonoBehaviour
{
    [SerializeField] private GameObject creditsMenu,mainMenu,instructionsMenu;
    // Start is called before the first frame update
    public void StartGame()
    {
        SceneManager.LoadScene(1);
        File.Delete(Application.dataPath + "/DataPersistence.json");
    }
    public void ToggleCredits()
    {
        if (mainMenu.activeInHierarchy)
        {
            creditsMenu.SetActive(true);
            mainMenu.SetActive(false);

            Debug.Log("Pressed Credits");
        }
        else
        {
            creditsMenu.SetActive(false);
            mainMenu.SetActive(true);
        }
    }

    public void ToggleInstructions()
    {
        if (mainMenu.activeInHierarchy)
        {
            instructionsMenu.SetActive(true);
            mainMenu.SetActive(false);

            Debug.Log("Pressed Instructions");
        }
        else
        {
            instructionsMenu.SetActive(false);
            mainMenu.SetActive(true);
        }
    }

    public void Quit()
    {
        Application.Quit();
    }
    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Player {
    public static Rigidbody body;   
    public static float maxMeters,
                sanity,
                jumpVal,
                speed; 
    public static Vector3 position;
    public static bool  won, lost;
    public static int gamesplayed;
}


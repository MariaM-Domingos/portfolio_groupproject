using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MiniGameHandler : MonoBehaviour
{
    private MetersHandler sanity;
    private DataPersistence data;
    [SerializeField] private float L,W;
    public int lives;
   
    
    public static float winVal, loseVal;
    // Start is called before the first frame update
    void Start() // if the player won other games, this is going to be set to false again
    {
        Player.won = false;
        Player.lost = false;
      
    }

    private void Update()
    {
        PlayerDies();
    }

    public void Win()
    {
        Debug.Log("won.");
        winVal = W;
        Player.won = true;
        
    }

    public void Lose()
    {
        Debug.Log("lost.");
        loseVal = L;
        Player.lost=true;
    }

    private void PlayerDies()
    {
        if (lives <= 0)
        {
            Debug.Log("lost.");
            loseVal = L;
            Player.lost = true;
            SceneManager.LoadScene(2);//load back to room
        }
    }

}

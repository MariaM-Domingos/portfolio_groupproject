﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*This class purelly checks if the inventory has any child 
 * besides 1, that is the panel
 * as the collectables are stored as childs of the slots
 * if they dont have any, make them automatically empty*/

public class Slot : MonoBehaviour
{
    [SerializeField] private int i;
    private PhoneHandler inventory;
    // Start is called before the first frame update
    void Start()
    {
        inventory = GameObject.FindGameObjectWithTag("Scripts").GetComponent<PhoneHandler>();
    }

    // Update is called once per frame
    void Update()
    {
            if (transform.childCount <=1)
            {
                inventory.isFull[i] = false;
            }
    }
}

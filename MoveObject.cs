﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour
{
    private GameObject player;
    private float distance;
    [SerializeField] private GameObject obj;
    [SerializeField] private float velocity;
    private Rigidbody _obj;
    //private MetersHandler _sanity;
    Vector3 objOrigin;
    private GameFinaleHandler finale;
    // Start is called before the first frame update

    void Start()
    {
        //_sanity = GameObject.FindGameObjectWithTag("Scripts").GetComponent<MetersHandler>();
        finale = GameObject.FindGameObjectWithTag("Scripts").GetComponent<GameFinaleHandler>();
        player = GameObject.FindGameObjectWithTag("Player");
        _obj = obj.GetComponent<Rigidbody>();
        velocity = 5;
        objOrigin = obj.gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(player.transform.position, obj.transform.position);
        
    }

    private void pushDirection()
    {
        Vector3 pushDir = (obj.transform.position - player.transform.position).normalized;
        if (Input.GetKey(KeyCode.E) && distance < 4)
        {
            _obj.isKinematic = false;
            _obj.AddForce(pushDir * velocity, ForceMode.Acceleration);

        }
        else
        {
            _obj.isKinematic = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if (Player.sanity <= 0.75)
            {
                pushDirection();
            }
            else
            {
                OpenDoor();
            }
        }         
    }

    private void OpenDoor()
    {
        if (Input.GetKey(KeyCode.E) && distance < 4)
        {
            // the player can open de door, door animaton.
            finale.GameWon();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            obj.transform.position = objOrigin;
        }
    }
}

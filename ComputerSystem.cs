﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class ComputerSystem : MonoBehaviour
{
    [SerializeField] private GameObject loading,mainScreen;
    private bool loadScene, canLoad;
    [SerializeField] private Slider FillInBar;
    private float timer;
    private bool isActive;
    private int sceneToLoad;

    void Start()
    {
        isActive = false;
        loadScene = false;
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    void OpenGame()
    {
     
    }
    public void GameToggle(int n)
    {
        sceneToLoad = n;
            isActive = !isActive;
            if (isActive)
            {
                Cursor.visible = isActive;
                Cursor.lockState = CursorLockMode.None;
                loading.SetActive(isActive);
                mainScreen.SetActive(false);
            if (!loadScene)
            {
                loadScene = true;
                StartCoroutine(LoadYourAsyncScene());
            }
            }
    }

    public void ReturnToRoon()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(2);
    }

    IEnumerator LoadYourAsyncScene()
    {
        while (timer < 1)
        {
            float randNumb = Random.Range(0, .3f);
            timer += randNumb;
            FillInBar.value += randNumb;
            randNumb = Random.Range(.2f, .5f);
            yield return new WaitForSeconds(randNumb);

        }

        if (timer > 1)
        {
            canLoad = true;
        }

        // Wait until the asynchronous scene fully at loads
        if (canLoad)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneToLoad);
            while (!asyncLoad.isDone)
            {

                yield return null;
            }
        }
    }
}

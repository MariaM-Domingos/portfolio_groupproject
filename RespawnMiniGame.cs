using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnMiniGame : MonoBehaviour
{
    private MiniGameHandler handler;
    private Vector3 playerInitialLocation;
    // Start is called before the first frame update
    void Start()
    {
        handler = GameObject.FindGameObjectWithTag("Scripts").GetComponent<MiniGameHandler>();
        playerInitialLocation = GameObject.FindGameObjectWithTag("Player").transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision) //respwan player
    {
        if (collision.gameObject.tag.Contains("Player"))
        {
            handler.lives -=1;
            collision.gameObject.transform.position = playerInitialLocation;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class TriggerScene : MonoBehaviour
{
    [SerializeField] private int sceneNumber;
    private MiniGameHandler handler;

    // Start is called before the first frame update
    void Start()
    {
        handler = GameObject.FindGameObjectWithTag("Scripts").GetComponent<MiniGameHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Contains("Player"))
        {
            if (gameObject.tag.Contains("Goal"))
            {
                handler.Win();
            }
            if (gameObject.tag == "Lose")
            {
                Debug.Log("Perdeu.");
                handler.Lose();
            }
            SceneManager.LoadScene(sceneNumber);
        }
    }
}

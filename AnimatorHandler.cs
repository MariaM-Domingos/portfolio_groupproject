﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorHandler : MonoBehaviour
{
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void SetWalking(bool condition)
    {
        animator.SetBool("isWalking", condition);    
    }
  
    public void SetJumping(bool condition)
    {
        animator.SetBool("isJumping", condition);
    }
    public void SetWalkingBwd(bool condition)
    {
        animator.SetBool("isWalkingBwd", condition);
    }
}

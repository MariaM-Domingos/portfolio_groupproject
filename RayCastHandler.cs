﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class RayCastHandler : MonoBehaviour
{
    //private string otherName;
    public Text objName;
    public RaycastHit hit;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetPointingObj();
    }

    public GameObject GetPointingObj()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag.Equals("Funcional"))
            {
                objName.text = hit.collider.name.ToString();
                return hit.transform.gameObject;
            }
        }
        return null;
    }
}

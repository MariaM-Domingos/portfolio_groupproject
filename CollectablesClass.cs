﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectablesClass : MonoBehaviour
{
    //public cuz they're acessed by DataPersistence
    public GameObject itemButton;
    public int code;
    [SerializeField] private float howFarAway;
    private PhoneHandler inventory;
    private float distance;
    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        inventory = GameObject.FindGameObjectWithTag("Scripts").GetComponent<PhoneHandler>();
        player = GameObject.FindGameObjectWithTag("Player");    
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(player.transform.position, this.transform.position);
    }

    private void OnMouseOver()
    {
        RaycastHit hit;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        if (Physics.Raycast(ray, out hit))
        {
            if (Input.GetKey(KeyCode.E) && distance < howFarAway)
            {
                if (hit.collider.tag.Equals("Collectable"))
                {
                    for (int i = 0; i < inventory.slots.Length; i++)
                    {
                        if (inventory.isFull[i] == false)
                        {
                            //Item can be added to Inventory!
                            inventory.isFull[i] = true;
                            Instantiate(itemButton, inventory.slots[i].transform, false);
                            inventory.itemsID[i] = code;
                            Destroy(hit.collider.gameObject);
                            break;
                        }
                    }
                }
            }
        }
        
    }
}

//https://www.flaticon.com/authors/nhor-phai ICONS AUTHOR
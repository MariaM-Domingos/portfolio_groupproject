using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableFuncionality : MonoBehaviour
{
    private MetersHandler _sanity;
    [SerializeField] private float valueToAdd;

    // Start is called before the first frame update
    void Start()
    {
        _sanity = GameObject.FindGameObjectWithTag("Scripts").GetComponent<MetersHandler>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UsePill()
    {
        _sanity.SetSanityMeter(valueToAdd);
        Destroy(gameObject);
    }
}

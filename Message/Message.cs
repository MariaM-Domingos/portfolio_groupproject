using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[System.Serializable]
public class Message
{
    [TextArea(3, 15)] public string text;
    public Text textObject;
    public bool hasReply;
    public MessageFrom person;
    public bool isCreated=false;

    public enum MessageFrom
    {
        player, 
        friend
    }
}

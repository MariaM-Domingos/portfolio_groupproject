﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "ContactFile", menuName = "Scriptables/ContactFile")]
public class ContactFile : ScriptableObject
{
    public string name;
    
    public Message[] dialogue;
    public string number;
    public Sprite profileImage;
    public int id;

}

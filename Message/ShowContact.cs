using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ShowContact : MonoBehaviour
{
    [SerializeField] private ContactFile contact;
    [SerializeField] private Image image;
    [SerializeField] private Text name;
    [SerializeField] private Text number;
    [SerializeField] private int contactID;
    [SerializeField] private GameObject messageScreen;
    [SerializeField] private GameObject contactScreen;
    [SerializeField] private GameObject centerDot;

    // Start is called before the first frame update
    void Start()
    {
        image.sprite = contact.profileImage;
        name.text = contact.name;
        number.text = contact.number;
        contactID = contact.id;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

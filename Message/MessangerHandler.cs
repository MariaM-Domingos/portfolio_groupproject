using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MessangerHandler : MonoBehaviour
{
    public GameObject[] messageScreen;
   
    [SerializeField] private ContactFile[] contact;
    [SerializeField] private List<Message> messageList = new List<Message>();
    [SerializeField] private int max;
    [SerializeField] private GameObject textObject;   
    [SerializeField] private GameObject[] chatPanel; // the content thing
    [SerializeField] private GameObject centerDot;
    [SerializeField] private float seconds;
    [SerializeField] private float sanityval;
    public Color player, friend;
    private int gamesPlayed;
    static List<int> momChoices = new List<int>();
    static List<int> friendChoices = new List<int>();
    static List<int> therapistChoices = new List<int>();

    [SerializeField] private bool[] hasPressed; // allow texts if variable true, allow if false

    // Start is called before the first frame update
    void Start()
    {
        hasPressed = new bool[3];
        gamesPlayed = Player.gamesplayed;
        
        //MOM
        momChoices.Add(0);
        LoadConversationsM(0);
        //FRIEND
        friendChoices.Add(0);
        LoadConversationsF(1);
        //THERAPIST
        therapistChoices.Add(0);
        LoadConversationsT(2);
        
    }

    // Update is called once per frame
    void Update()
    {
        RespondOrNotMOM(0);
        RespondOrNotFRIERND(1);
        RespondOrNotTHERAPIST(2);
    }

    void LoadConversationsF(int i)
    {
        for (int a = 0; a < friendChoices.Count; a++)
        {
            if (contact[i].dialogue[friendChoices[a]].person == Message.MessageFrom.player)
            {
                SendMessageToChat(contact[i].dialogue[friendChoices[a]].text, Message.MessageFrom.player, i);
            }
            if (contact[i].dialogue[friendChoices[a]].person == Message.MessageFrom.friend)
            {
                SendMessageToChat(contact[i].dialogue[friendChoices[a]].text, Message.MessageFrom.friend, i);
            }
        }
    }
    void LoadConversationsM(int i)
    {
        for (int a = 0; a < momChoices.Count; a++)
        {
            if (contact[i].dialogue[momChoices[a]].person == Message.MessageFrom.player)
            {
                SendMessageToChat(contact[i].dialogue[momChoices[a]].text, Message.MessageFrom.player, i);
            }
            if (contact[i].dialogue[momChoices[a]].person == Message.MessageFrom.friend)
            {
                SendMessageToChat(contact[i].dialogue[momChoices[a]].text, Message.MessageFrom.friend, i);
            }
        }
    }
    void LoadConversationsT(int i)
    {
        for (int a = 0; a < therapistChoices.Count; a++)
        {
            if (contact[i].dialogue[therapistChoices[a]].person == Message.MessageFrom.player)
            {
                SendMessageToChat(contact[i].dialogue[therapistChoices[a]].text, Message.MessageFrom.player, i);
            }
            if (contact[i].dialogue[therapistChoices[a]].person == Message.MessageFrom.friend)
            {
                SendMessageToChat(contact[i].dialogue[therapistChoices[a]].text, Message.MessageFrom.friend, i);
            }
        }
    }

    public void LoadTexts(int cID)
    {
        messageScreen[cID].SetActive(true);

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            messageScreen[cID].SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1;
            centerDot.SetActive(true);
            Cursor.visible = false;
        }
    }

    void SendMessageToChat(string text, Message.MessageFrom person, int i)
    {
        if (messageList.Count >= max)
        {
            Destroy(messageList[0].textObject.gameObject);
            messageList.Remove(messageList[0]); // remove the 1st element but adds one at the end
        }

        Message newMessage = new Message();
        newMessage.text = text;

     
            // add text to the panel
            GameObject newText = Instantiate(this.textObject, chatPanel[i].transform);

        newMessage.textObject = newText.GetComponent<Text>();
        newMessage.textObject.text = newMessage.text;

        newMessage.textObject.color = MessageColor(person);
    }

    //gets who is who to chose the color
    Color MessageColor(Message.MessageFrom person)
    {
        Color color = friend; //starts as the friend

        switch (person)
        {
            case Message.MessageFrom.player:
                color = player;
                break;
        }

        return color;
    }

    void RespondOrNotMOM(int i)
    {
        if (messageScreen[i].activeInHierarchy == true)
        {
            // 0 GAMES PLAYED
            if (Input.GetKeyDown(KeyCode.R) && gamesPlayed == 0 && hasPressed[i] == false)
            {
                SendMessageToChat(contact[i].dialogue[1].text, Message.MessageFrom.player, i);
                //index 0 1 
                momChoices.Add(1);
                hasPressed[i] = true;
            }
            else if (Input.GetKeyDown(KeyCode.I) && gamesPlayed == 0 && hasPressed[i] == false)
            {
                messageScreen[i].SetActive(false);
                //never leaves index 0
                hasPressed[i] = true;
            }

            // 1 GAME PLAYED
            if (Input.GetKeyDown(KeyCode.R) && gamesPlayed == 1 && hasPressed[i] == false)
            {
                SendMessageToChat(contact[i].dialogue[2].text, Message.MessageFrom.friend, i);
                //index 0 1 2
                momChoices.Add(2);
                hasPressed[i] = true;
            }
            else if (Input.GetKeyDown(KeyCode.I) && gamesPlayed == 1 && hasPressed[i] == false)
            {
                hasPressed[i] = true;
                return;
            }

            // 1 or MORE GAME PLAYED
            if (Input.GetKeyDown(KeyCode.R) && gamesPlayed > 1 && hasPressed[i] == false)
            {
                hasPressed[i] = true;
                return;                
            }
            else if (Input.GetKeyDown(KeyCode.I) && gamesPlayed > 1 && hasPressed[i] == false)
            {
                hasPressed[i] = true;
                return;
            }
        }
    }

    void RespondOrNotFRIERND(int i)
    {
        
        if (messageScreen[i].activeInHierarchy == true)  //Friends page open
        {
            // 0 GAMES PLAYED
            if(Input.GetKeyDown(KeyCode.R) && gamesPlayed == 0 && hasPressed[i] == false)
            {
                SendMessageToChat(contact[i].dialogue[1].text, Message.MessageFrom.player, i);
                //index 0 1 
                friendChoices.Add(1);
                hasPressed[i] = true;
            } else if(Input.GetKeyDown(KeyCode.I) && gamesPlayed == 0 && hasPressed[i] == false)
            {
                messageScreen[i].SetActive(false);
                friendChoices.Add(3);
                hasPressed[i] = true;
            }

            // 1 GAME PLAYED
            if (Input.GetKeyDown(KeyCode.R) && gamesPlayed == 1 && hasPressed[i] == false)
            {
                SendMessageToChat(contact[i].dialogue[4].text, Message.MessageFrom.player, i);
                //index 0 1 2 4 5
                friendChoices.Add(2);
                friendChoices.Add(4);
                friendChoices.Add(5);
                hasPressed[i] = true;
            }
            else if (Input.GetKeyDown(KeyCode.I) && gamesPlayed == 1 && hasPressed[i] == false)
            {
                //SendMessageToChat(contact[i].dialogue[3].text, Message.MessageFrom.friend, i);
                friendChoices.Add(6);
                hasPressed[i] = true;
            }

            // 2 GAMES PLAYED
            if (Input.GetKeyDown(KeyCode.R) && gamesPlayed == 2 && hasPressed[i] == false)
            {
                SendMessageToChat(contact[i].dialogue[7].text, Message.MessageFrom.player, i);
                //index 0 1 2 4 5 7 8 9 10 11 12
                friendChoices.Add(7);
                friendChoices.Add(8);
                friendChoices.Add(9);
                friendChoices.Add(10);
                friendChoices.Add(11);
                friendChoices.Add(12);
                hasPressed[i] = true;
            }
            else if (Input.GetKeyDown(KeyCode.I) && gamesPlayed == 2 && hasPressed[i] == false)
            {
                //SendMessageToChat(contact[i].dialogue[3].text, Message.MessageFrom.friend, i);;
                hasPressed[i] = true;
                return;
                
            }
            // 2 or MORE  GAMES PLAYED
            if (Input.GetKeyDown(KeyCode.R) && gamesPlayed > 2 && hasPressed[i] == false)
            {
                hasPressed[i] = true;
                return;
            }
            else if (Input.GetKeyDown(KeyCode.I) && gamesPlayed > 2 && hasPressed[i] == false)
            {
                hasPressed[i] = true;
                return;
            }
        }
    }

    void RespondOrNotTHERAPIST(int i)
    {
        if (messageScreen[i].activeInHierarchy == true)
        {
            // 0 GAMES PLAYED
            if (Input.GetKeyDown(KeyCode.R) && gamesPlayed == 0 && hasPressed[i] == false)
            {
                SendMessageToChat(contact[i].dialogue[1].text, Message.MessageFrom.player, i);
                this.Wait(seconds, () =>
                {
                    SendMessageToChat(contact[i].dialogue[2].text, Message.MessageFrom.friend, i);
                    this.Wait(seconds, () =>
                    {
                        SendMessageToChat(contact[i].dialogue[3].text, Message.MessageFrom.player, i);
                        this.Wait(seconds, () =>
                        {
                            SendMessageToChat(contact[i].dialogue[4].text, Message.MessageFrom.friend, i);
                            this.Wait(seconds, () =>
                            {
                                SendMessageToChat(contact[i].dialogue[5].text, Message.MessageFrom.player, i);
                            });
                        });
                    });
                });
                //index 0 1 2 3 4 5 6
                therapistChoices.Add(1);
                therapistChoices.Add(2);
                therapistChoices.Add(3);
                therapistChoices.Add(4);
                therapistChoices.Add(5);
                therapistChoices.Add(6);
                hasPressed[i] = true;
            }
            else if (Input.GetKeyDown(KeyCode.I) && gamesPlayed == 0 && hasPressed[i] == false)
            {
                messageScreen[i].SetActive(false);
                therapistChoices.Add(6);
                hasPressed[i] = true;
            }

            // 1 GAME PLAYED
            if (Input.GetKeyDown(KeyCode.R) && gamesPlayed == 1 && hasPressed[i] == false)
            {
                SendMessageToChat(contact[i].dialogue[7].text, Message.MessageFrom.player, i);
                //index 0 1 2 3 4 5 6 7 8 
                therapistChoices.Add(7);
                therapistChoices.Add(8);
                hasPressed[i] = true;
            }
            else if (Input.GetKeyDown(KeyCode.I) && gamesPlayed == 1 && hasPressed[i] == false)
            {
                hasPressed[i] = true;
                return;
            }

            //1 or MORE GAMES
            if (Input.GetKeyDown(KeyCode.R) && gamesPlayed > 1 && hasPressed[i] == false)
            {
                SendMessageToChat(contact[i].dialogue[7].text, Message.MessageFrom.player, i);
                //index 0 1 2 3 4 5 6 7 8 
                therapistChoices.Add(7);
                therapistChoices.Add(8);
                hasPressed[i] = true;
            }
            else if (Input.GetKeyDown(KeyCode.I) && gamesPlayed > 1 && hasPressed[i] == false)
            {
                hasPressed[i] = true;
                return;
            }
        }
          
    
    }
       
    void ChoiseMade()
    {
        // make text appear saying that you already chose your letter
    }
 }


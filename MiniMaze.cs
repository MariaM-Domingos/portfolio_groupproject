using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MiniMaze : MonoBehaviour
{
    [SerializeField] private GameObject[] pickups;
    public static int valCollected;
    private int totalPickups;
    [SerializeField] private Text collected;
    [SerializeField] private Text total;
    [SerializeField] private float timeLeft;
    private float currentTime;
    [SerializeField] private Text timer;
    private MiniGameHandler handler;
    [SerializeField] private GameObject toDisappear;

    // Start is called before the first frame update
    void Start()
    {
        handler = GameObject.FindGameObjectWithTag("Scripts").GetComponent<MiniGameHandler>();
        pickups = GameObject.FindGameObjectsWithTag("Maze Pickup");
        valCollected = 0;
        currentTime = timeLeft;
        totalPickups = pickups.Length;
    }

    // Update is called once per frame
    void Update()
    {
        collected.text = valCollected.ToString();
        total.text = totalPickups.ToString();
        TimeCount();
        PlayerDies();
        MakeDisappear();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Goal" && valCollected == totalPickups)
        {
            handler.Win();
            SceneManager.LoadScene(2);
        }
    }

    private void TimeCount()
    {
        currentTime -= 1 * Time.deltaTime;
        timer.text = currentTime.ToString("0s");
    }

    private void PlayerDies()
    {
        if (currentTime <= 0)
        {
            handler.Lose();
            SceneManager.LoadScene(2);//load back to room
        }
    }

    private void MakeDisappear()
    {
        if (valCollected == totalPickups)
        {
            Destroy(toDisappear);
        }
    }
}

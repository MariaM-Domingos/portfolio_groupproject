using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlappyBird : MonoBehaviour
{
    [SerializeField] float moveValue;
    [SerializeField] private float jumpValue;
    [SerializeField] private GameObject objectMove;
    [SerializeField] private GameObject startPanel;
    private PauseMenuHandler paused;
    private MiniGameHandler handler;

    // Start is called before the first frame update
    void Start()
    {
        Player.jumpVal = jumpValue;
        Player.body = GetComponent<Rigidbody>();
        paused = GameObject.FindGameObjectWithTag("Scripts").GetComponent<PauseMenuHandler>();

        //starts paused
        Time.timeScale = 0;
        Cursor.visible = true;
        handler = GameObject.FindGameObjectWithTag("Scripts").GetComponent<MiniGameHandler>();

    }

    // Update is called once per frame
    void Update()
    {
        GameStart();
        BirdMovement();
    }

    void BirdMovement()
    {
        transform.Translate(Vector3.forward * moveValue * Time.deltaTime);
        objectMove.transform.position = new Vector3(transform.position.x, objectMove.transform.position.y, objectMove.transform.position.z);

        //move up  space
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Player.body.AddForce(Vector3.up * Player.jumpVal, ForceMode.Impulse);
        }
    }

    void GameStart()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Time.timeScale = 1;
            startPanel.SetActive(false);
            Cursor.visible = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Goal")
        {
            handler.Win();
            SceneManager.LoadScene(2);
        }
    }
}

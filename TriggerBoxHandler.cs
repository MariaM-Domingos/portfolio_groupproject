using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBoxHandler : MonoBehaviour
{
    [SerializeField] private GameObject obj;
    [SerializeField] private float xValue;
    [SerializeField] private float time;
    private Vector3 originalPos;
    private bool beenthere;
    private PlayerJump player;
    // Start is called before the first frame update
    void Start()
    {
        beenthere = false;
        originalPos = obj.transform.position;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerJump>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Player") && beenthere== false)
        {   
            obj.transform.Translate(new Vector3(xValue,0,0));
            Invoke("ReturnToOriginalPosition", time);
            Destroy(obj.gameObject);
            beenthere = true;
            player.SetJumping(true);
        }
    }

    private void ReturnToOriginalPosition()
    {
        obj.transform.position = originalPos;
    }
}

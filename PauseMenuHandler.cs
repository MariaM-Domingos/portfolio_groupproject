﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuHandler : MonoBehaviour
{
    private bool paused;
    [SerializeField] private GameObject pauseMenu;

    void Start()
    {
        paused = false;
    }

    // Update is called once per frame
    void Update()
    {
        PauseButtonPress();
    }

    void PauseButtonPress()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && paused == false)
        {
            PauseToggle();
        }
        else { return; }
    }

    public void PauseToggle()
    {

        paused = !paused;
        if (paused) //if pause is true
        {
            Cursor.visible = paused;
            Cursor.lockState = CursorLockMode.None;
            pauseMenu.SetActive(paused);
            Time.timeScale = 0;
        }
        else
        {
            Cursor.visible = paused;
            Cursor.lockState = CursorLockMode.None;
            pauseMenu.SetActive(paused);
            Time.timeScale = 1;
        }
    }
    public void ReturnToPc()
    {
        SceneManager.LoadScene(3);
    }

    public bool GetPaused() { return paused; }
}

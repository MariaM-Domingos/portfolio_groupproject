﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUp : MonoBehaviour
{
    [SerializeField] private Transform itemLoc;
    [SerializeField] private float speed;
    [SerializeField] private float howFarAway;
    [SerializeField] private GameObject text;
    private float distance;
    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        speed = 5f;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(player.transform.position, this.transform.position);
        DropItem();
    }

    void DropItem()
    {
        if (Input.GetKeyUp(KeyCode.E))
        {
            this.transform.parent = null;
            GetComponent<Rigidbody>().useGravity = true;
        }
    }
    //rotate object
    private void OnMouseDrag()
    {
        float XaxisRotation = Input.GetAxis("Mouse X") * speed;
        float YaxisRotation = Input.GetAxis("Mouse Y") * speed;

        transform.Rotate(Vector3.down, XaxisRotation);
        transform.Rotate(Vector3.right, YaxisRotation);
    }

    private void OnMouseOver()
    {
        
        RaycastHit hit;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        if (Physics.Raycast(ray, out hit))
        {
            if ( distance < howFarAway)
            {
                if (Input.GetKey(KeyCode.E) && hit.collider.tag.Equals("Items"))
                {
                    hit.collider.GetComponent<Rigidbody>().useGravity = false;
                    hit.collider.transform.position = itemLoc.position;
                    hit.collider.transform.parent = GameObject.Find("ItemLocation").transform;
                }
            }
        }
    }
}

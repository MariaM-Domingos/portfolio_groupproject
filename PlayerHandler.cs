﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerHandler : MonoBehaviour
{
    private AnimatorHandler animator;
    [SerializeField] float walking;
    [SerializeField] float running;

    void Start()
    {
        Player.maxMeters = 1f;
        Player.sanity = 0.5f;
        Player.gamesplayed = 0;
        animator = gameObject.GetComponent<AnimatorHandler>();
    }

    void Update()
    {
        PlayerPosition();
        PlayerRunning();
    }
    void PlayerMovement()
    {
        //move foward W
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * Player.speed * Time.deltaTime);
            animator.SetWalking(true);
        }
        else
        {
            animator.SetWalking(false);
        }
        //move backward S
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back * Player.speed * Time.deltaTime );
            animator.SetWalkingBwd(true);
        }
        else
        {
            animator.SetWalkingBwd(false);
        }

        //move to left A
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * Player.speed * Time.deltaTime);
        }
        //move to right D
        
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * Player.speed * Time.deltaTime);
        }
    }
    void PlayerRunning()
    {
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.W))
        {
            Player.speed = running; 
            PlayerMovement();
        }
        else
        {
            Player.speed = walking;
            PlayerMovement();
        }
    }
    void PlayerPosition()
    {
        Player.position = transform.position;
    }
   
}
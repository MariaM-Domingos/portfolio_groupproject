using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
[CreateAssetMenu(fileName = "LightScript" ,menuName ="Scriptables/LightScript",order =1)]
public class LightScrip : ScriptableObject
{

    public Gradient ambientColor;
    public Gradient directionalColor;
    public Gradient fogColor;
}

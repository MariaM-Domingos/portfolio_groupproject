using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LightHandler : MonoBehaviour
{
    [SerializeField] private Light[] _light;
    [SerializeField] private LightScrip scrip;
    [SerializeField, Range(0, 1)] private float sanity; //relataed to the sanity meter

    private MetersHandler _sanity;
    // Start is called before the first frame update


    void Start()
    {
        _sanity = GameObject.FindGameObjectWithTag("Scripts").GetComponent<MetersHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        if (scrip == null)
            return;

        sanity = _sanity.GetPlayerSanity();
        UpdateLightning(sanity);
    }

    private void UpdateLightning(float value)
    {
        RenderSettings.ambientLight = scrip.ambientColor.Evaluate(value);
        RenderSettings.fogColor = scrip.fogColor.Evaluate(value);
        for (int i = 0; i < _light.Length; i++)
        {
            if (_light[i] != null)
            {
                _light[i].color = scrip.directionalColor.Evaluate(value);
            }
        }
       
    }
}

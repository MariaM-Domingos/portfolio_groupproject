﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    //private Player player;
    private AnimatorHandler animator;
    private bool jumping;
    [SerializeField] private float jumpValue;

    // Start is called before the first frame update
    void Start()
    {
        Player.jumpVal = jumpValue; 
        Player.body = GetComponent<Rigidbody>();
        animator = gameObject.GetComponent<AnimatorHandler>();
        jumping = true;
    }

    // Update is called once per frame
    void Update()
    {
        Jump();
    }
    
    void Jump()
    {
        //move up  space
        if (Input.GetKeyDown(KeyCode.Space) && jumping)
        {
            Player.body.AddForce(Vector3.up * Player.jumpVal, ForceMode.Impulse);
            animator.SetJumping(true);
            jumping = false;
        }
        else
        {
            animator.SetJumping(false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Structure")
        {
            jumping = true;
        }
    }

    public void SetJumping(bool jump)
    {
        jumping = jump;
    }
}
